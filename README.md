# gnome-keyring-default-unlock

A simple C program that uses gnome-keyring API to unlock default keyring. Can be used to unlock the keyring on login in non-standard setups.

## Prerequisites

* gnome-keyring development files and dependencies
* pkg-config
* gcc

## Build

```bash
    make
```

## Install

```bash
    sudo make install
```

## Contributing
Although there is little to contribute in such a small piece of software, issues and pull requests are welcome.

## Enjoy!
