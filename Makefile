PREFIX=/usr
all:
	gcc -o gnome-keyring-default-unlock gnome-keyring-default-unlock.c `pkg-config --cflags --libs gnome-keyring-1`
install:
	cp gnome-keyring-default-unlock ${PREFIX}/bin/
