#include <gnome-keyring.h>

int main(void) {
    char* default_keyring;
    GnomeKeyringResult result;
    result = gnome_keyring_get_default_keyring_sync(&default_keyring);
    if(result == GNOME_KEYRING_RESULT_OK) {
        gnome_keyring_unlock_sync(default_keyring, NULL);
    };
    return 0;
}
